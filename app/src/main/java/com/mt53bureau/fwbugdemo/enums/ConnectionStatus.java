package com.mt53bureau.fwbugdemo.enums;

public enum ConnectionStatus {
    CONNECTED,
    DISCONNECTED,
    UNKNOWN
}

