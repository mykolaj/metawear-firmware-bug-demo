package com.mt53bureau.fwbugdemo.datahandle;

import android.os.Handler;
import android.os.HandlerThread;

import com.mbientlab.metawear.MetaWearBoard;
import com.mt53bureau.fwbugdemo.BuildConfig;
import com.mt53bureau.fwbugdemo.Trace;
import com.mt53bureau.fwbugdemo.model.BoardRecordWrapper;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;


public class CsvWriter {
    private static final String LOG_TAG = String.format("%s.%s", BuildConfig.LOG_TAG, CsvWriter.class.getSimpleName());
    private final Handler writerHandler;
    private final Queue<BoardRecordWrapper> recordQueue = new LinkedList<>();
    private final AtomicBoolean isIdle = new AtomicBoolean(true);
    private final AtomicBoolean shouldStop = new AtomicBoolean(false);
    private final MetaWearBoard board;
    private final File file;
    private OutputStream outputStream;

    public CsvWriter(final MetaWearBoard board, final File file) {
        this.board = board;
        this.file = file;
        final HandlerThread thread = new HandlerThread("csv-write-thread-" + board.getMacAddress());
        thread.start();
        this.writerHandler = new Handler(thread.getLooper());
    }

    public void addRecord(BoardRecordWrapper recordWrapper) {
        if (recordWrapper != null) {
            recordQueue.add(recordWrapper);
        }
    }

    public void processQueue() {
        if (!isIdle.get()) {
            return;
        }
        isIdle.set(false);
        BoardRecordWrapper command = recordQueue.poll();
        while (command != null) {
            final BoardRecordWrapper finalCommand = command;
            writerHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (shouldStop.get()) {
                        return;
                    }
                    writeToFile(String.format("%s\n", finalCommand.toCsv()));
                }
            });
            command = recordQueue.poll();
        }
        isIdle.set(true);
        if (shouldStop.get()) {
            closeOutputStream();
        }
    }

    private void writeToFile(final String str) {
        if (str == null || str.length() == 0) {
            return;
        }
        if (outputStream == null) {
            return;
        }
        try {
            outputStream.write(str.getBytes("UTF-8"));
        } catch (IOException e) {
            Trace.e(LOG_TAG + " writeToFile: " + board.getMacAddress()
                    + " " + ExceptionUtils.getMessage(e));
        }
    }

    public void stop() {
        shouldStop.set(true);
        writerHandler.removeCallbacksAndMessages(null);
        writerHandler.getLooper().quit();

        if (isIdle.get()) {
            closeOutputStream();
        }
    }

    private void openOutputStream() {
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " opening output stream" );
        if (outputStream != null) {
            throw new IllegalStateException(LOG_TAG + ": output stream is already opened."
                    + " Close it first before opening a new one");
        }
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(file, false));
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " output stream opened" );
        } catch (IOException e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to open file for board. "
                    + ExceptionUtils.getMessage(e));
        }
    }

    private void closeOutputStream() {
        Trace.d(LOG_TAG + " closeOutputStream: " + board.getMacAddress() + " closing output stream");
        if (outputStream == null) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stream is already closed");
            return;
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            Trace.w(LOG_TAG + ": " + board.getMacAddress() + " " + ExceptionUtils.getMessage(e));
        }
        outputStream = null;
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stream closed");
    }

    public void prepare() {
        openOutputStream();
        writeToFile(String.format("%s\n", board.getMacAddress()));
        writeToFile("AccX,AccY,AccZ,GyrX,GyrY,GyrZ,MagX,MagY,MagZ\n");
    }
}
