package com.mt53bureau.fwbugdemo.datahandle;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.RouteManager;
import com.mt53bureau.fwbugdemo.Trace;
import com.mt53bureau.fwbugdemo.services.MetaWearStreamingService;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class MagnetometerHandler extends AsyncOperation.CompletionHandler<RouteManager> {
    private final MetaWearBoard board;
    private final RouteManager.MessageHandler messageHandler;
    private int routeId;

    public MagnetometerHandler(final MetaWearBoard board, final RouteManager.MessageHandler messageHandler) {
        this.board = board;
        this.messageHandler = messageHandler;
    }

    @Override
    public void success(RouteManager result) {
        routeId = result.id();
        result.subscribe(MetaWearStreamingService.MAGNETOMETER_STREAM_KEY, messageHandler);
    }

    @Override
    public void failure(final Throwable error) {
        Trace.e("error committing an magnetometer route for board " + board.getMacAddress() + ". "
                + ExceptionUtils.getMessage(error));
    }

    public int getRouteId() {
        return routeId;
    }
}
