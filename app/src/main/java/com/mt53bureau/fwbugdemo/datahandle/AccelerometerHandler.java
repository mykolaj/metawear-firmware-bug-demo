package com.mt53bureau.fwbugdemo.datahandle;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.RouteManager;
import com.mt53bureau.fwbugdemo.Trace;
import com.mt53bureau.fwbugdemo.services.MetaWearStreamingService;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Handles Accelerometer stream
 */
public class AccelerometerHandler extends AsyncOperation.CompletionHandler<RouteManager> {

    private final MetaWearBoard board;
    private final RouteManager.MessageHandler messageHandler;
    private int routeId = -1;

    public AccelerometerHandler(final MetaWearBoard board, final RouteManager.MessageHandler messageHandler) {
        this.board = board;
        this.messageHandler = messageHandler;
    }

    @Override
    public void success(RouteManager result) {
        routeId = result.id();
        result.subscribe(MetaWearStreamingService.ACCELEROMETER_STREAM_KEY, messageHandler);
    }

    @Override
    public void failure(final Throwable e) {
        Trace.e("error committing an accelerometer route for board " + board.getMacAddress() + ". "
                + ExceptionUtils.getMessage(e));
    }

    public int getRouteId() {
        return routeId;
    }
}
