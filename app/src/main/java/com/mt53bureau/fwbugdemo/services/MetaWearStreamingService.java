package com.mt53bureau.fwbugdemo.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.MetaWearBleService;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.Bmi160Accelerometer;
import com.mbientlab.metawear.module.Bmi160Gyro;
import com.mbientlab.metawear.module.Bmm150Magnetometer;
import com.mbientlab.metawear.module.Gyro;
import com.mt53bureau.fwbugdemo.BuildConfig;
import com.mt53bureau.fwbugdemo.Trace;
import com.mt53bureau.fwbugdemo.datahandle.BoardConnectionHandler;
import com.mt53bureau.fwbugdemo.datahandle.CsvWriter;
import com.mt53bureau.fwbugdemo.enums.ConnectionStatus;
import com.mt53bureau.fwbugdemo.events.BatteryUpdateEvent;
import com.mt53bureau.fwbugdemo.events.BoardAddedToListEvent;
import com.mt53bureau.fwbugdemo.events.BoardConnectedEvent;
import com.mt53bureau.fwbugdemo.events.BoardDisconnectedEvent;
import com.mt53bureau.fwbugdemo.events.BoardRemovedFromList;
import com.mt53bureau.fwbugdemo.events.ConnectionStateChangedReport;
import com.mt53bureau.fwbugdemo.events.DataRecordingStartedEvent;
import com.mt53bureau.fwbugdemo.events.DataRecordingStoppedEvent;
import com.mt53bureau.fwbugdemo.fileio.FileIo;
import com.mt53bureau.fwbugdemo.model.BoardReading;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;


public class MetaWearStreamingService extends Service {

    public static final String ACCELEROMETER_STREAM_KEY = "accelerometer_stream";
    public static final String GYROSCOPE_STREAM_KEY = "gyroscope_stream";
    public static final String MAGNETOMETER_STREAM_KEY = "magnetometer_stream";
    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + MetaWearStreamingService.class.getSimpleName();
    private static final long BOARD_RECONNECT_TIMEOUT_MILLIS = 1500;
    private static final Class<Accelerometer> ACCELEROMETER_CLASS = Accelerometer.class /* Bmi160Accelerometer.class */;
    private static final String NO_ACCELEROMETER_MESSAGE = "failed to find " + ACCELEROMETER_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private static final Class<Gyro> GYROSCOPE_CLASS = Gyro.class /* Bmi160Gyro.class */;
    private static final String NO_GYROSCOPE_MESSAGE = "failed to find " + GYROSCOPE_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private static final Class<Bmm150Magnetometer> MAGNETOMETER_CLASS = Bmm150Magnetometer.class;
    private static final String NO_MAGNETOMETER_MESSAGE = "failed to find " + MAGNETOMETER_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private final MetaWearReaderServiceBinder binder = new MetaWearReaderServiceBinder();
    private final MetaWearServiceConnection metaWearServiceConnection = new MetaWearServiceConnection();

    /** Keeps track a connection state of devices (for UI purposes) */
    private final Map<String, BoardStateInfo> connectedBoardsInfo = new ConcurrentHashMap<>();
    /** Keeps track which slot is assigned to every device */
    private final Map<String, Integer> deviceToSlotMapping = new ConcurrentHashMap<>();
    /** Keeps track which devices are selected by a user */
    private final Map<String, MetaWearBoard> selectedBoards = new ConcurrentHashMap<>();

    private final Map<String, BoardConnectionHandler> boardConnectionHandlers = new ConcurrentHashMap<>();
    private final AtomicBoolean isDataRecordingInProgress = new AtomicBoolean(false);

    private MetaWearBleService.LocalBinder metaWearServiceBinder;
    private BluetoothAdapter btAdapter;

    /** Main thread's Handler */
    private Handler mainHandler;

    /** Keeps CPU running while data recording is in progress */
    private PowerManager.WakeLock wakeLock;

    public static void start(Context context) {
        Intent intent = new Intent(context, MetaWearStreamingService.class);
        context.startService(intent);
    }

    public static void stop(final Context context) {
        context.stopService(new Intent(context, MetaWearStreamingService.class));
    }

    /**
     * @return Bluetooth addresses of currently selected boards
     */
    public Collection<String> getSelectedBoards() {
        return selectedBoards.keySet();
    }

    public Collection<BoardStateInfo> getConnectedBoardsInfo() {
        return new ArrayList<>(connectedBoardsInfo.values());
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mainHandler = new Handler();
        bindService(new Intent(this, MetaWearBleService.class), metaWearServiceConnection, BIND_AUTO_CREATE);
        btAdapter = ((BluetoothManager) getSystemService(BLUETOOTH_SERVICE)).getAdapter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(metaWearServiceConnection);
        stopDataRecording();
        for (BoardConnectionHandler ch : new ArrayList<>(boardConnectionHandlers.values())) {
            ch.stop();
        }
        if (wakeLock != null) {
            try {
                wakeLock.release();
            } catch (Exception e) {
                Trace.w(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Trace.w(LOG_TAG + "onLowMemory: TODO Immediately flush all in-memory data into a CSV file");
        // TODO Immediately flush all in-memory data into a CSV file
    }


    /**
     * Add a device to a list of selected devices, and tries to establish a connection to it right away
     *
     * @param address
     */
    public void selectDevice(final String address) {
        if (metaWearServiceBinder == null) {
            Trace.d(LOG_TAG + ": somehow " + MetaWearBleService.class.getSimpleName()
                    + " is not bound.");
            return;
        }

        if (isDataRecordingInProgress.get()) {
            return;
        }

        if (TextUtils.isEmpty(address)) {
            return;
        }

        final BluetoothDevice device = btAdapter.getRemoteDevice(address);
        final MetaWearBoard board = metaWearServiceBinder.getMetaWearBoard(device);
        if (board == null) {
            Trace.e(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " returned null" +
                    " for a bluetooth device with address \'" + device.getAddress() + "\'.");
            return;
        }

        // Get slot number
        final int number = getFreeSlot(address);

        if (!selectedBoards.containsKey(board.getMacAddress())) {
            selectedBoards.put(board.getMacAddress(), board);

            // Notify that a new device were added to a list of selected devices
            EventBus.getDefault().post(new BoardAddedToListEvent(address, number));

            connectBoard(board, false);
        }

        if (!deviceToSlotMapping.containsKey(address)) {
            deviceToSlotMapping.put(address, number);
        }
    }

    /**
     * Remove a device from a selected devices list, and disconnects from it if any connection
     * is established.
     *
     * @param address
     */
    public void unselectDevice(final String address) {
        if (TextUtils.isEmpty(address)) {
            return;
        }

        if (isDataRecordingInProgress.get()) {
            return;
        }

        final MetaWearBoard board = selectedBoards.remove(address);
        deviceToSlotMapping.remove(address);

        if (board != null) {
            disconnectBoard(board);
        }

        // Notify that a device is removed from a list of selected devices
        EventBus.getDefault().post(new BoardRemovedFromList(address));
    }

    /**
     * Start collecting a data from all devices which are in a selected devices list
     */
    public void startDataRecording() {
        if (!isDataRecordingInProgress.get()) {
            isDataRecordingInProgress.set(true);
        } else {
            Trace.w(LOG_TAG + ": detected attempt to star a data recording when a recording is already in progress");
            return;
        }

        // Prevent CPU from going into sleep mode while data capturing is active
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                String.format("%s-WakeLock", BuildConfig.APPLICATION_ID));
        wakeLock.acquire();

        // Go into a background thread to handle possible UI blocking operations
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (final BoardConnectionHandler ch : new ArrayList<>(boardConnectionHandlers.values())) {
                    final MetaWearBoard board = ch.getBoard();
                    final File file = FileIo.prepareOutputFile(MetaWearStreamingService.this, board.getMacAddress());
                    final CsvWriter csvWriter = new CsvWriter(board, file);
                    csvWriter.prepare();
                    ch.setCsvWriter(csvWriter);
                    doBoardSensorsConnect(board);
                }

                EventBus.getDefault().post(new DataRecordingStartedEvent());
            }
        }).start();
    }

    /**
     * Drop all connection to all devices, and stop a data recording
     */
    public void stopDataRecording() {
        if (!isDataRecordingInProgress.get()) {
            Trace.w(LOG_TAG + ": detected attempt to stop a data recording when there isn't any recording in progress");
            return;
        }
        // Disconnect from a board's modules but leave the board connected
        for (BoardConnectionHandler ch : new ArrayList<>(boardConnectionHandlers.values())) {
            ch.stopAccelerometerStreaming();
            ch.stopGyroscopeStreaming();
            ch.stopMagnetometerStreaming();

            final CsvWriter csvWriter = ch.getCsvWriter();
            if (csvWriter != null) {
                csvWriter.stop();
            }
        }

        isDataRecordingInProgress.set(false);
        EventBus.getDefault().post(new DataRecordingStoppedEvent());

        // Release a WakeLock to allow a device to go into a sleep mode
        if (wakeLock != null) {
            try {
                wakeLock.release();
            } catch (Exception e) {
                Trace.w(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
            }
        }
    }

    /**
     * Connects to a given board and reads its battery state.
     * This function starts to collect a data from the board if the data collection is currently enabled.
     * If the data collection is disabled then this function just establishes the connection and
     * subscribes for a battery state readings.
     *
     * @param board
     */
    private void connectBoard(final MetaWearBoard board, final boolean startStreamingOnConnect) {
        if (board == null) {
            return;
        }
        final BoardConnectionHandler.BoardConnectionListener listener = new BoardConnectionHandler.BoardConnectionListener() {
            @Override
            public void onBoardConnected(final MetaWearBoard board) {
                readBatteryState(board);
                notifyBoardConnectionStatus(board, true);
                connectedBoardsInfo.put(board.getMacAddress(),
                        new BoardStateInfo(board, ConnectionStatus.CONNECTED));
                if (startStreamingOnConnect) {
                    doBoardSensorsConnect(board);
                }
            }

            @Override
            public void onBoardDisconnected(final MetaWearBoard board) {
                // Refresh a board's state info
                final BoardStateInfo boardStateInfo = connectedBoardsInfo.get(board.getMacAddress());
                if (boardStateInfo != null) {
                    boardStateInfo.connectionStatus = ConnectionStatus.DISCONNECTED;
                } else {
                    connectedBoardsInfo.put(board.getMacAddress(),
                            new BoardStateInfo(board, ConnectionStatus.DISCONNECTED));
                }

                notifyBoardConnectionStatus(board, false);

                // Check if this board is still selected
                boolean selected = isBoardSelected(board);
                if (selected) {
                    reconnect(board);
                } else {
                    doBoardDisconnect(board);
                }
            }

            @Override
            public void onConnectionFailed(final MetaWearBoard board) {
                // Refresh a board's state info
                final BoardStateInfo boardStateInfo = connectedBoardsInfo.get(board.getMacAddress());
                if (boardStateInfo != null) {
                    boardStateInfo.connectionStatus = ConnectionStatus.DISCONNECTED;
                } else {
                    connectedBoardsInfo.put(board.getMacAddress(),
                            new BoardStateInfo(board, ConnectionStatus.DISCONNECTED));
                }

                notifyBoardConnectionStatus(board, false);
                boolean selected = isBoardSelected(board);
                if (selected) {
                    reconnect(board);
                } else {
                    doBoardDisconnect(board);
                }
            }
        };

        final BoardConnectionHandler connectionHandler = new BoardConnectionHandler(board);
        connectionHandler.setListener(listener);
        boardConnectionHandlers.put(board.getMacAddress(), connectionHandler);
        connectionHandler.connect();
    }

    private boolean isBoardSelected(final MetaWearBoard board) {
        return selectedBoards.containsKey(board.getMacAddress());
    }

    private void disconnectBoard(final MetaWearBoard board) {
        if (board == null) {
            return;
        }

        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.remove(board.getMacAddress());
        if (connectionHandler != null) {
            connectionHandler.stop();
        }
    }

    protected void doBoardSensorsConnect(final MetaWearBoard board) {
        if (board == null) {
            return;
        }

        final Accelerometer accelerometer = retrieveAccelerometer(board);
        if (accelerometer != null) {
            final boolean status = startAccelerometer(board, accelerometer);
            if (status) {
                startAccelerometerStreaming(board, accelerometer);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_ACCELEROMETER_MESSAGE);
        }

        final Gyro gyroModule = retrieveGyroscope(board);
        if (gyroModule != null) {
            final boolean status = startGyroscope(board, gyroModule);
            if (status) {
                startGyroscopeStreaming(board, gyroModule);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_GYROSCOPE_MESSAGE);
        }

        final Bmm150Magnetometer magnetometer = retrieveMagnetometer(board);
        if (magnetometer != null) {
            final boolean status = startMagnetometer(board, magnetometer);
            if (status) {
                startMagnetometerStreaming(board, magnetometer);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_MAGNETOMETER_MESSAGE);
        }
    }

    protected void doBoardDisconnect(MetaWearBoard board) {
        if (board == null) {
            return;
        }

        disconnectBoard(board);
    }

    private void readBatteryState(final MetaWearBoard board) {
        board.readBatteryLevel().onComplete(new AsyncOperation.CompletionHandler<Byte>() {
            @Override
            public void success(final Byte result) {
                EventBus.getDefault().post(new BatteryUpdateEvent(board.getMacAddress(), result));
                Trace.d(LOG_TAG + ": " + board.getMacAddress() + " battery state received." +
                        " Charge=" + result);
            }

            @Override
            public void failure(final Throwable error) {
                Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to read a battery state."
                        + ExceptionUtils.getMessage(error));
            }
        });
    }

    private void notifyBoardConnectionStatus(final MetaWearBoard board, boolean isConnected) {
        if (board == null) {
            return;
        }

        final String address = board.getMacAddress();

        // TODO There are two types of events which is confusing. Leave only one of them in the program.

        final ConnectionStateChangedReport event = new ConnectionStateChangedReport(address,
                isConnected ? ConnectionStatus.CONNECTED : ConnectionStatus.DISCONNECTED);
        EventBus.getDefault().post(event);
        if (isConnected) {
            EventBus.getDefault().post(new BoardConnectedEvent(address));
        } else {
            EventBus.getDefault().post(new BoardDisconnectedEvent(address));
        }
    }

    private boolean startAccelerometer(final MetaWearBoard board, final Accelerometer accelerometer) {
        if (accelerometer == null) {
            throw new IllegalStateException(LOG_TAG + ": accelerometer module must not be null");
        }
        accelerometer.enableAxisSampling();
        accelerometer.setAxisSamplingRange(Bmi160Accelerometer.AccRange.AR_16G.scale());
        accelerometer.setOutputDataRate(Bmi160Accelerometer.OutputDataRate.ODR_50_HZ.frequency());
        try {
            accelerometer.start();
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " "
                    + ACCELEROMETER_CLASS.getSimpleName() + " started");
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        return true;
    }

    private void startAccelerometerStreaming(final MetaWearBoard board, Accelerometer accelerometer) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startAccelerometerStreaming(accelerometer);
    }

    protected void stopAccelerometer(final MetaWearBoard board) {
        boolean success = false;
        final Accelerometer accelerometer = retrieveAccelerometer(board);
        if (accelerometer == null) {
            return;
        }

        try {
            accelerometer.disableAxisSampling();
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stopAccelerometer: "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        try {
            accelerometer.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stopAccelerometer: "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d((LOG_TAG + ": " + board.getMacAddress() + " "
                    + ACCELEROMETER_CLASS.getSimpleName() + " stopped"));
        }
    }

    private void startGyroscopeStreaming(final MetaWearBoard board, final Gyro gyroModule) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startGyroscopeStreaming(gyroModule);
    }

    private boolean startGyroscope(final MetaWearBoard board, final Gyro gyroModule) {
        if (gyroModule == null) {
            throw new IllegalStateException(LOG_TAG + ": gyroscope module must not be null");
        }

        try {
            gyroModule.setAngularRateRange(Bmi160Gyro.FullScaleRange.FSR_500.scale());
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " " + ExceptionUtils.getMessage(e));
        }
        try {
            gyroModule.setOutputDataRate(50.0f);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " " + ExceptionUtils.getMessage(e));
        }
        try {
            gyroModule.start();
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                + " started");
        return true;
    }

    protected void stopGyroscope(final MetaWearBoard board) {
        boolean success = false;
        Gyro gyroModule = retrieveGyroscope(board);

        if (gyroModule == null) {
            return;
        }

        try {
            gyroModule.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopGyroscope: "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " stopped");
        }
    }

    private boolean startMagnetometer(final MetaWearBoard board, final Bmm150Magnetometer magnetometer) {
        if (magnetometer == null) {
            throw new IllegalStateException(LOG_TAG + ": magnetometer module must not be null");
        }
        try {
            // Set to low power mode
            magnetometer.setPowerPreset(Bmm150Magnetometer.PowerPreset.LOW_POWER);
        } catch (Exception e) {
            Trace.e(LOG_TAG + board.getMacAddress() + " startMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + " " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.enableBFieldSampling();
        } catch (Exception e) {
            Trace.e(LOG_TAG + board.getMacAddress() + " startMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + " " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.start();
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + MAGNETOMETER_CLASS.getSimpleName()
                + " started" );
        return true;
    }

    private void startMagnetometerStreaming(final MetaWearBoard board, final Bmm150Magnetometer magnetometer) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startMagnetometerStreaming(magnetometer);
    }

    protected void stopMagnetometer(final MetaWearBoard board) {
        boolean success = false;
        Bmm150Magnetometer magnetometer = retrieveMagnetometer(board);

        if (magnetometer == null) {
            return;
        }

        try {
            magnetometer.disableBFieldSampling();
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " "
                    + MAGNETOMETER_CLASS.getSimpleName() + " stopped");
        }
    }


    @Nullable
    private Accelerometer retrieveAccelerometer(final MetaWearBoard board) {
        Accelerometer accelerometer;
        try {
            accelerometer = board.getModule(ACCELEROMETER_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": "+ board.getMacAddress() + " failed to get "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }

        return accelerometer;
    }

    @Nullable
    private Gyro retrieveGyroscope(final MetaWearBoard board) {
        Gyro gyroModule;
        try {
            gyroModule = board.getModule(GYROSCOPE_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to get "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }

        return gyroModule;
    }

    @Nullable
    private Bmm150Magnetometer retrieveMagnetometer(final MetaWearBoard board) {
        Bmm150Magnetometer magnetometer;
        try {
            magnetometer = board.getModule(MAGNETOMETER_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to get "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }
        return magnetometer;
    }

    private int getFreeSlot(final String address) {
        if (deviceToSlotMapping.isEmpty()) {
            return 1;
        }

        if (deviceToSlotMapping.containsKey(address)) {
            return deviceToSlotMapping.get(address);
        }

        final Collection<Integer> allSlots = deviceToSlotMapping.values();
        final Integer maxNumber = Collections.max(allSlots);

        for (int i = 1; i < maxNumber + 1; i++) {
            if (!allSlots.contains(i)) {
                return i;
            }
        }

        return maxNumber + 1;
    }

    private void reconnect(final MetaWearBoard board) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        if (connectionHandler == null) {
            return;
        }

        mainHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Trace.d(LOG_TAG + ": " + board.getMacAddress() + " reconnect attempt ");
                connectionHandler.connect();
                doBoardSensorsConnect(board);
            }
        }, BOARD_RECONNECT_TIMEOUT_MILLIS);

        Trace.d(LOG_TAG + ": board " + board.getMacAddress() + " reconnect is scheduled in "
                + String.valueOf((float) BOARD_RECONNECT_TIMEOUT_MILLIS / 1000.0f) + " seconds");
    }

    public boolean isDataRecordingInProgress() {
        return isDataRecordingInProgress.get();
    }

    private static int[] calculateTimeSpans(int timeFractionMillis, final float dataFrequency) {
        final int count = Float.valueOf(dataFrequency).intValue();
        int currentTick = timeFractionMillis;
        final int[] timeFractions = new int[count];

        for (int i = 0; i < count || currentTick < 1000; i++) {
            currentTick += timeFractionMillis;
            timeFractions[i] = currentTick;
        }
        return timeFractions;
    }

    public static class BoardStateInfo {
        final MetaWearBoard board;
        ConnectionStatus connectionStatus = ConnectionStatus.UNKNOWN;

        public BoardStateInfo(final MetaWearBoard board) {
            this(board, ConnectionStatus.UNKNOWN);
        }

        public BoardStateInfo(final MetaWearBoard board, final ConnectionStatus status) {
            this.board = board;
            connectionStatus = status;
        }

        public MetaWearBoard getBoard() {
            return board;
        }

        public ConnectionStatus getConnectionStatus() {
            return connectionStatus;
        }
    }

    private class MetaWearServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName componentName, final IBinder iBinder) {
            metaWearServiceBinder = (MetaWearBleService.LocalBinder) iBinder;
            metaWearServiceBinder.executeOnUiThread();
            Trace.d(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " connected");
        }

        @Override
        public void onServiceDisconnected(final ComponentName componentName) {
            metaWearServiceBinder = null;
            Trace.d(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " disconnected");
        }
    }

    public class MetaWearReaderServiceBinder extends Binder {
        private final MetaWearStreamingService service = MetaWearStreamingService.this;

        public final MetaWearStreamingService getService() {
            return service;
        }
    }

    public static class BoardRecordBundle {
        private final long sessionId;
        private Date timestamp;
        private final Map<String, BoardReading> boardRecords = new HashMap<>();

        public BoardRecordBundle(final long sessionId) {
            this.sessionId = sessionId;
        }

        public void addRecord(BoardReading record) {
            if (record == null) {
                return;
            }
            boardRecords.put(record.getBoardAddress(), record);
        }

        public Map<String, BoardReading> getBoardRecords() {
            return boardRecords;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(final Date timestamp) {
            this.timestamp = timestamp;
        }

        public long getSessionId() {
            return sessionId;
        }
    }

}

