package com.mt53bureau.fwbugdemo.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;

import com.mt53bureau.fwbugdemo.BuildConfig;
import com.mt53bureau.fwbugdemo.R;
import com.mt53bureau.fwbugdemo.Trace;
import com.mt53bureau.fwbugdemo.services.MetaWearStreamingService;
import com.mt53bureau.fwbugdemo.ui.ble.BleScannerFragment;

import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
public class MainActivity extends AppCompatActivity implements BleScannerFragment.ScannerFragmentHost {
    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + MainActivity.class.getSimpleName();
    private ReaderServiceConnection readerServiceConnection = new ReaderServiceConnection();
    private CoordinatorLayout coordinatorLayout;
    private MetaWearStreamingService readerService;
    private CompoundButton startRecordSwitch;
    private OnStartRecordSwitchListener onStartRecordSwitchListener;

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.root_view_coordinator);
        startRecordSwitch = (CompoundButton) findViewById(R.id.start_record_switch);
        onStartRecordSwitchListener = new OnStartRecordSwitchListener();
        startRecordSwitch.setOnCheckedChangeListener(onStartRecordSwitchListener);
        setupFragmentUi();
        MetaWearStreamingService.start(this);
        bindService(new Intent(this, MetaWearStreamingService.class), readerServiceConnection, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(readerServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_start_bt_connect:
                if (readerService == null) {
                    return true;
                }
                if (readerService.isDataRecordingInProgress()) {
                    toggleDataReading(false);
                    setStartRecordSwitchChecked_NoTrigger(false);
                } else {
                    toggleDataReading(false);
                    setStartRecordSwitchChecked_NoTrigger(true);
                }
                return true;

            case R.id.action_exit:
                toggleDataReading(false);
                MetaWearStreamingService.stop(this);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        if (readerService != null) {
            MenuItem item = menu.findItem(R.id.action_start_bt_connect);
            if (readerService.isDataRecordingInProgress()) {
                item.setTitle(R.string.action_bt_disconnect);
            } else {
                item.setTitle(R.string.action_bt_connect);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void setupFragmentUi() {
        final FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(BleScannerFragment.TAG);
        if (fragment != null) {
            fm.beginTransaction().attach(fragment).commit();
        } else {
            fragment = BleScannerFragment.newInstance();
            fm.beginTransaction().add(R.id.container, fragment, BleScannerFragment.TAG).commit();
        }
    }

    @Override
    public void onDeviceChecked(final String address, final boolean isChecked) {
        if (isChecked) {
            Trace.d(LOG_TAG + ": device checked \'" + address + "\'");
            readerService.selectDevice(address);
        } else {
            Trace.d(LOG_TAG + ": device unchecked \'" + address + "\'");
            readerService.unselectDevice(address);
        }
    }

    @Override
    public void onUserRefusedEnableBluetooth() {
        // TODO Show information dialog with OK button. Finish activity only when the button is clicked
        if (!BuildConfig.DEBUG) {
            finish();
        }
    }

    @Override
    public void onBluetoothNotSupported() {
        // TODO Show information dialog with OK button. Finish activity only when the button is clicked
        if (!BuildConfig.DEBUG) {
            finish();
        }
    }

    @Override
    public void onBluetoothEnabled() {

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        // Notify support-fragments about activity result.
        // It's crazy to figure out why this isn't done automatically.
        final List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * @param turnOn
     */
    private void toggleDataReading(boolean turnOn) {
        if (readerService == null) {
            return;
        }
        if (readerService.isDataRecordingInProgress()) {
            if (!turnOn) {
                readerService.stopDataRecording();
            }
        } else {
            if (turnOn) {
                readerService.startDataRecording();
            }
        }
        invalidateOptionsMenu();
    }

    private void setStartRecordSwitchChecked_NoTrigger(final boolean isChecked) {
        if (startRecordSwitch == null) {
            return;
        }
        startRecordSwitch.setOnCheckedChangeListener(null);
        startRecordSwitch.setChecked(isChecked);
        startRecordSwitch.setOnCheckedChangeListener(onStartRecordSwitchListener);
    }

    private class ReaderServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            readerService = ((MetaWearStreamingService.MetaWearReaderServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            readerService = null;
        }

    }

    private class OnStartRecordSwitchListener implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
            if (readerService == null) {
                return;
            }
            if (readerService.isDataRecordingInProgress()) {
                if (!isChecked) {
                    toggleDataReading(false);
                }
            } else {
                if (isChecked) {
                    toggleDataReading(true);
                }
            }
        }
    }
}
