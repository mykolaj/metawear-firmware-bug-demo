package com.mt53bureau.fwbugdemo.ui.ble;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mt53bureau.fwbugdemo.R;
import com.mt53bureau.fwbugdemo.enums.ConnectionStatus;
import com.mt53bureau.fwbugdemo.model.ItemInfoWrapper;
import com.mt53bureau.fwbugdemo.model.ScannedDeviceInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class DeviceInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int RSSI_BAR_LEVELS = 5;
    private static final int RSSI_BAR_SCALE = 100 / RSSI_BAR_LEVELS;
    private static final int VIEW_TYPE_BLE_ITEM = 1;
    private static final int VIEW_TYPE_FOOTER = 2;
    private final List<ItemInfoWrapper> mData = new ArrayList<>();
    private final DeviceCheckedListenerInternal mDeviceCheckedListenerInternal = new DeviceCheckedListenerInternal();
    private final OnScanClickListenerInternal mOnScanClickListenerInternal = new OnScanClickListenerInternal();
    private final OnDeviceCheckListener mOnDeviceCheckListener;
    private final String mBatteryLabel;
    private final String mSlotLabel;
    private OnAdapterActionsListener mOnAdapterActionsListener;
    private RecyclerView recyclerView;
    private boolean isScanInProgress;
    private boolean isScanButtonEnabled = true;

    public DeviceInfoAdapter(Context context, final OnDeviceCheckListener listener) {
        mOnDeviceCheckListener = listener;
        final Resources res = context.getResources();
        mBatteryLabel = res.getString(R.string.battery_label);
        mSlotLabel = res.getString(R.string.slot_label);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_BLE_ITEM) {
            final View itemView = inflater.inflate(R.layout.list_item_ble_scan_entry, parent, false);
            return new BleItemViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_FOOTER) {
            final View itemView = inflater.inflate(R.layout.list_item_ble_scan_footer, parent, false);
            return new FooterViewHolder(itemView);
        } else {
            throw new IllegalStateException(DeviceInfoAdapter.class.getSimpleName() + ": wrong view type");
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_BLE_ITEM:
                bindBleItemView((BleItemViewHolder) holder, position);
                break;
            case VIEW_TYPE_FOOTER:
                bindFooterView((FooterViewHolder) holder, position);
                break;
            default:
                break;
        }

    }

    private void bindFooterView(final FooterViewHolder holder, final int position) {
        // Refresh button label
        if (isScanInProgress) {
            holder.btnScan.setText(R.string.ble_scan_cancel);
        } else {
            holder.btnScan.setText(R.string.ble_scan_start);
        }
        holder.btnScan.setEnabled(isScanButtonEnabled);
    }

    private void bindBleItemView(final BleItemViewHolder holder, final int position) {
        final ItemInfoWrapper itemInfoWrapper = mData.get(position);
        final ScannedDeviceInfo scannedDeviceInfo = itemInfoWrapper.getScannedDeviceInfo();
        final String deviceName = scannedDeviceInfo.getBluetoothName();
        final String bluetoothAddress = scannedDeviceInfo.getBluetoothAddress();
        holder.deviceAddress.setText(bluetoothAddress);
        //
        if (!TextUtils.isEmpty(deviceName)) {
            holder.deviceName.setText(deviceName);
        } else {
            holder.deviceName.setText(R.string.label_unknown_device_name);
        }
        //
        final int rssi = scannedDeviceInfo.getRssi();
        holder.deviceRSSI.setText(String.format(Locale.US, "%3d dBm", rssi));
        holder.rssiChart.setImageLevel(Math.min(RSSI_BAR_LEVELS - 1, (127 + rssi + 5) / RSSI_BAR_SCALE));
        //
        holder.check.setTag(bluetoothAddress);
        final boolean isChecked = itemInfoWrapper.isChecked;
        holder.check.setOnCheckedChangeListener(null);
        holder.check.setChecked(isChecked);
        holder.check.setOnCheckedChangeListener(mDeviceCheckedListenerInternal);
        holder.check.setEnabled(itemInfoWrapper.allowCheckboxInput);
        if (isChecked) {
            holder.batteryCharge.setText(String.format(Locale.US, mBatteryLabel + ": %2d %%", itemInfoWrapper.batteryCharge));
            holder.connectionSlotNumber.setText(String.format(Locale.US, mSlotLabel + ": %2d", itemInfoWrapper.slotNumber));
        } else {
            holder.batteryCharge.setText("");
            holder.connectionSlotNumber.setText("");
        }
        if (itemInfoWrapper.slotNumber <= 0) {
            holder.connectionSlotNumber.setText("");
        }
        final ConnectionStatus connectionStatus = itemInfoWrapper.connectionStatus;
        if (connectionStatus != null && connectionStatus == ConnectionStatus.CONNECTED) {
            holder.connectionStatus.setText(R.string.device_state_connected_label);
        } else {
            holder.connectionStatus.setText(R.string.device_state_disconnected_label);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size() + 1;
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    public void update(ScannedDeviceInfo newInfo) {
        updateInternal(newInfo, true);
    }

    private void updateInternal(ScannedDeviceInfo newInfo, boolean notifyDataSetChanged) {
        int pos = getDeviceInfoPosition(newInfo.getBluetoothAddress());

        if (pos == -1) {
            mData.add(new ItemInfoWrapper(newInfo));
        } else {
            final ScannedDeviceInfo deviceInfo = mData.get(pos).getScannedDeviceInfo();
            deviceInfo.setRssi(newInfo.getRssi());
        }
        if (notifyDataSetChanged) {
            notifyDataSetChanged();
        }
    }

    private void updateBatteryInternal(String address, byte battery, boolean notifyDataSetChanged) {
        int pos = getDeviceInfoPosition(address);
        if (pos != -1) {
            mData.get(pos).batteryCharge = battery;
            if (notifyDataSetChanged) {
                notifyDataSetChanged();
            }
        }
    }

    public void updateBattery(String address, byte battery) {
        updateBatteryInternal(address, battery, true);
    }

    private void updateConnectionStatusInternal(String address, ConnectionStatus connectionStatus, boolean notifyDataSetChanged) {
        int pos = getDeviceInfoPosition(address);
        if (pos != -1) {
            mData.get(pos).connectionStatus = connectionStatus;
            if (notifyDataSetChanged) {
                notifyDataSetChanged();
            }
        }
    }

    public void updateConnectionStatus(String address, ConnectionStatus connectionStatus) {
        updateConnectionStatusInternal(address, connectionStatus, true);
    }

    private void updateSlotNumberInternal(String address, int slotNumber, boolean notifyDataSetChanged) {
        int pos = getDeviceInfoPosition(address);
        if (pos != -1) {
            mData.get(pos).slotNumber = slotNumber;
            if (notifyDataSetChanged) {
                notifyDataSetChanged();
            }
        }
    }

    public void updateSlotNumber(String address, int slotNumber) {
        updateSlotNumberInternal(address, slotNumber, true);
    }

    /**
     *
     * @param addresses A collection of addresses of the devices to set checkboxes checked for
     */
    public void updateCheckedState(Collection<String> addresses) {
        if (addresses == null || addresses.isEmpty()) {
            return;
        }
        for (String address : addresses) {
            int pos = getDeviceInfoPosition(address);
            if (pos != -1) {
                mData.get(pos).isChecked = true;
            }
        }
        notifyDataSetChanged();
    }

    private int getDeviceInfoPosition(final String address) {
        int pos = -1;
        for (int i = 0; i < mData.size(); i++) {
            final ItemInfoWrapper wrapper = mData.get(i);
            final String a = wrapper.getScannedDeviceInfo().getBluetoothAddress();
            if (a.equals(address)) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    public List<ItemInfoWrapper> getDeviceList() {
        return new ArrayList<>(mData);
    }

    public void update(final List<ItemInfoWrapper> deviceList) {
        if (deviceList == null || deviceList.isEmpty()) {
            return;
        }
        for (ItemInfoWrapper wrapper : deviceList) {
            final ScannedDeviceInfo scannedDeviceInfo = wrapper.getScannedDeviceInfo();
            final String address = scannedDeviceInfo.getBluetoothAddress();
            updateInternal(scannedDeviceInfo, false);
            updateSlotNumberInternal(address, wrapper.slotNumber, false);
            updateConnectionStatusInternal(address, wrapper.connectionStatus, false);
            updateBatteryInternal(address, wrapper.batteryCharge, false);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(final int position) {
        if (position < mData.size()) {
            return VIEW_TYPE_BLE_ITEM;
        } else {
            return VIEW_TYPE_FOOTER;
        }
    }

    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    public OnAdapterActionsListener getOnAdapterActionsListener() {
        return mOnAdapterActionsListener;
    }

    public void setOnAdapterActionsListener(final OnAdapterActionsListener mOnAdapterActionsListener) {
        this.mOnAdapterActionsListener = mOnAdapterActionsListener;
    }

    public void updateScanButton(final boolean isScanning) {
        isScanInProgress = isScanning;
        notifyDataSetChanged();
    }

    public void enableCheckboxInput(final boolean allowInput) {
        for (ItemInfoWrapper wrapper : mData) {
            wrapper.allowCheckboxInput = allowInput;
        }
        notifyDataSetChanged();
    }

    public void enableScanButtonInput(final boolean allowInput) {
        isScanButtonEnabled = allowInput;
        notifyDataSetChanged();
    }

    public interface OnDeviceCheckListener {
        void onDeviceChecked(final String address);
        void onDeviceUnchecked(final String address);
    }

    public interface OnAdapterActionsListener {
        void onScanButtonClick();
    }

    /* package */ class BleItemViewHolder extends RecyclerView.ViewHolder {
        public final TextView deviceAddress;
        public final TextView deviceName;
        public final TextView deviceRSSI;
        public final TextView batteryCharge;
        public final TextView connectionSlotNumber;
        public final TextView connectionStatus;
        public final CompoundButton check;
        public final ImageView rssiChart;

        public BleItemViewHolder(final View view) {
            super(view);
            deviceAddress = (TextView) view.findViewById(R.id.text_device_address);
            deviceName = (TextView) view.findViewById(R.id.text_device_name);
            deviceRSSI = (TextView) view.findViewById(R.id.ble_rssi_value);
            rssiChart = (ImageView) view.findViewById(R.id.ble_rssi_png);
            batteryCharge = (TextView) view.findViewById(R.id.text_battery_charge);
            connectionSlotNumber = (TextView) view.findViewById(R.id.text_slot_number);
            connectionStatus = (TextView) view.findViewById(R.id.text_connection_status);
            check = (CompoundButton) view.findViewById(R.id.check);
            check.setOnCheckedChangeListener(mDeviceCheckedListenerInternal);
        }
    }

    /* package */ class FooterViewHolder extends RecyclerView.ViewHolder {

        public final Button btnScan;

        public FooterViewHolder(final View view) {
            super(view);
            btnScan = (Button) view.findViewById(R.id.btn_scan);
            btnScan.setOnClickListener(mOnScanClickListenerInternal);
        }
    }

    private class DeviceCheckedListenerInternal implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
            final String address = (String) buttonView.getTag();
            final int position = getDeviceInfoPosition(address);
            if (position != -1) {
                mData.get(position).isChecked = isChecked;
            }
            if (mOnDeviceCheckListener != null) {
                if (isChecked) {
                    mOnDeviceCheckListener.onDeviceChecked(address);
                } else {
                    mOnDeviceCheckListener.onDeviceUnchecked(address);
                }
            }
        }
    }

    private class OnScanClickListenerInternal implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            if (mOnAdapterActionsListener != null) {
                mOnAdapterActionsListener.onScanButtonClick();
            }
        }
    }
}
