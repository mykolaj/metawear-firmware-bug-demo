package com.mt53bureau.fwbugdemo.events;

public class BatteryUpdateEvent extends DeviceStateChangedReport {

    private final byte mBatteryLevel;

    public BatteryUpdateEvent(final String deviceAddress, byte batteryLevel) {
        super(deviceAddress);
        mBatteryLevel = batteryLevel;
    }

    public byte getBatteryLevel() {
        return mBatteryLevel;
    }
}
