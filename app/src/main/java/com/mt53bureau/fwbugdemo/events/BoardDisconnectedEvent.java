package com.mt53bureau.fwbugdemo.events;

public class BoardDisconnectedEvent extends DeviceStateChangedReport {

    public BoardDisconnectedEvent(final String deviceAddress) {
        super(deviceAddress);
    }
}
