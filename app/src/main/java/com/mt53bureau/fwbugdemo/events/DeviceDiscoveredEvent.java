package com.mt53bureau.fwbugdemo.events;


import com.mt53bureau.fwbugdemo.model.ScannedDeviceInfo;

/**
 * Notifies when a new Bluetooth device gets discovered
 */
public class DeviceDiscoveredEvent {

    private final ScannedDeviceInfo deviceInfo;

    public DeviceDiscoveredEvent(ScannedDeviceInfo info) {
        deviceInfo = info;
    }

    public ScannedDeviceInfo getDeviceInfo() {
        return deviceInfo;
    }
}
