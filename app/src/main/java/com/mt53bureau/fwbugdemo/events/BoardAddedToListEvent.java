package com.mt53bureau.fwbugdemo.events;


public class BoardAddedToListEvent extends DeviceStateChangedReport {

    private final int mSlotNumber;

    public BoardAddedToListEvent(final String deviceAddress, final int slotNumber) {
        super(deviceAddress);
        mSlotNumber = slotNumber;
    }

    public int getSlotNumber() {
        return mSlotNumber;
    }
}
