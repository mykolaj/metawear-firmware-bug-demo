package com.mt53bureau.fwbugdemo.events;

public class BoardConnectedEvent extends DeviceStateChangedReport {

    public BoardConnectedEvent(final String deviceAddress) {
        super(deviceAddress);
    }
}
