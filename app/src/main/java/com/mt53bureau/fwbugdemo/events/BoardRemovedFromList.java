package com.mt53bureau.fwbugdemo.events;

public class BoardRemovedFromList extends DeviceStateChangedReport {

    public BoardRemovedFromList(final String deviceAddress) {
        super(deviceAddress);
    }
}
