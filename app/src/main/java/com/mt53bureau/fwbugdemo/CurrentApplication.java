package com.mt53bureau.fwbugdemo;

import android.support.multidex.MultiDexApplication;

import net.danlew.android.joda.JodaTimeAndroid;

import org.greenrobot.eventbus.EventBus;

@SuppressWarnings("FieldCanBeLocal")
public class CurrentApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        EventBus.builder().throwSubscriberException(false).installDefaultEventBus();
    }

}
