package com.mt53bureau.fwbugdemo;

import android.annotation.SuppressLint;
import android.util.Log;

@SuppressLint("LongLogTag")
public class Trace {

    public static void d(String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(BuildConfig.APPLICATION_ID, msg);
        }

    }

    public static void e(String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(BuildConfig.APPLICATION_ID, msg);
        }

    }

    public static void w(String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(BuildConfig.APPLICATION_ID, msg);
        }

    }

    public static void i(String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(BuildConfig.APPLICATION_ID, msg);
        }

    }

}
