package com.mt53bureau.fwbugdemo.fileio;

import android.content.Context;

import com.mt53bureau.fwbugdemo.BuildConfig;
import com.mt53bureau.fwbugdemo.Trace;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import java.io.File;

public class FileIo {
    public static final String ILLEGAL_FILE_NAME_REGEX = "[^a-zA-Z0-9.-]";
    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + FileIo.class.getSimpleName();

    @SuppressWarnings("SpellCheckingInspection")
    public static final String FILES_DIR = "MultiMetawearDemo";

    public static File prepareOutputFile(final Context context, final String fileName) {
        final Storage storage = SimpleStorage.getExternalStorage();
        final boolean exists = storage.isDirectoryExists(FileIo.FILES_DIR);
        if (!exists) {
            final boolean status = storage.createDirectory(FileIo.FILES_DIR);
            if (!status) {
                Trace.e(LOG_TAG + ": failed to create directory " + FileIo.FILES_DIR);
            } else {
                Trace.d(LOG_TAG + ": directory " + FileIo.FILES_DIR + " created successfully");
            }
        }
        final String name = fileName.replace(ILLEGAL_FILE_NAME_REGEX, "_").trim();
        final String file = name + ".csv";
        if (!storage.isFileExist(FileIo.FILES_DIR, file)) {
            storage.createFile(FileIo.FILES_DIR, file, "");
        }
        return storage.getFile(FileIo.FILES_DIR, file);
    }

}
