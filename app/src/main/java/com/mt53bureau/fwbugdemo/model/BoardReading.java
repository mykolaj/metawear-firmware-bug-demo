package com.mt53bureau.fwbugdemo.model;

import android.support.annotation.NonNull;

import com.mt53bureau.fwbugdemo.CsvFormatters;

import java.util.Date;
import java.util.Locale;

import static com.mt53bureau.fwbugdemo.CsvFormatters.FLOAT_FORMATTER;

public class BoardReading {

    private Long id;
    private String boardAddress;
    private Date timestamp;
    private Float accX;
    private Float accY;
    private Float accZ;
    private Float gyrX;
    private Float gyrY;
    private Float gyrZ;
    private Float magX;
    private Float magY;
    private Float magZ;

    public BoardReading() {
    }

    public BoardReading(Long id, String boardAddress, java.util.Date timestamp, Float accX, Float accY, Float accZ, Float gyrX, Float gyrY, Float gyrZ, Float magX, Float magY, Float magZ) {
        this.id = id;
        this.boardAddress = boardAddress;
        this.timestamp = timestamp;
        this.accX = accX;
        this.accY = accY;
        this.accZ = accZ;
        this.gyrX = gyrX;
        this.gyrY = gyrY;
        this.gyrZ = gyrZ;
        this.magX = magX;
        this.magY = magY;
        this.magZ = magZ;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBoardAddress() {
        return boardAddress;
    }

    public void setBoardAddress(@NonNull String boardAddress) {
        this.boardAddress = boardAddress;
    }

    public java.util.Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NonNull java.util.Date timestamp) {
        this.timestamp = timestamp;
    }

    public Float getAccX() {
        return accX;
    }

    public void setAccX(Float accX) {
        this.accX = accX;
    }

    public Float getAccY() {
        return accY;
    }

    public void setAccY(Float accY) {
        this.accY = accY;
    }

    public Float getAccZ() {
        return accZ;
    }

    public void setAccZ(Float accZ) {
        this.accZ = accZ;
    }

    public Float getGyrX() {
        return gyrX;
    }

    public void setGyrX(Float gyrX) {
        this.gyrX = gyrX;
    }

    public Float getGyrY() {
        return gyrY;
    }

    public void setGyrY(Float gyrY) {
        this.gyrY = gyrY;
    }

    public Float getGyrZ() {
        return gyrZ;
    }

    public void setGyrZ(Float gyrZ) {
        this.gyrZ = gyrZ;
    }

    public Float getMagX() {
        return magX;
    }

    public void setMagX(Float magX) {
        this.magX = magX;
    }

    public Float getMagY() {
        return magY;
    }

    public void setMagY(Float magY) {
        this.magY = magY;
    }

    public Float getMagZ() {
        return magZ;
    }

    public void setMagZ(Float magZ) {
        this.magZ = magZ;
    }

    public static String toCsv(BoardReading reading) {
        if (reading == null) {
            return ",,,,,,,,"; // "acc-x,acc-y,acc-z,gyr-x,gyr-y,gyr-z,mag-x,mag-y,mag-z"
        }
        return String.format(Locale.US, "%s,%s,%s,%s,%s,%s,%s,%s,%s",
                reading.accX != null ? FLOAT_FORMATTER.format(reading.accX) : "",
                reading.accY != null ? FLOAT_FORMATTER.format(reading.accY) : "",
                reading.accZ != null ? FLOAT_FORMATTER.format(reading.accZ) : "",
                reading.gyrX != null ? FLOAT_FORMATTER.format(reading.gyrX) : "",
                reading.gyrY != null ? FLOAT_FORMATTER.format(reading.gyrY) : "",
                reading.gyrZ != null ? FLOAT_FORMATTER.format(reading.gyrZ) : "",
                reading.magX != null ? FLOAT_FORMATTER.format(reading.magX) : "",
                reading.magY != null ? FLOAT_FORMATTER.format(reading.magY) : "",
                reading.magZ != null ? FLOAT_FORMATTER.format(reading.magZ) : ""
        );
    }

}
