package com.mt53bureau.fwbugdemo.model;

import android.os.Parcel;

public class ScannedDeviceInfo extends BtDeviceInfo {
    private int rssi;

    public ScannedDeviceInfo(String address, String name, int rssi) {
        super(address, name);
        this.rssi = rssi;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(final int rssi) {
        this.rssi = rssi;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.rssi);
    }

    protected ScannedDeviceInfo(Parcel in) {
        super(in);
        this.rssi = in.readInt();
    }

    public static final Creator<ScannedDeviceInfo> CREATOR = new Creator<ScannedDeviceInfo>() {
        @Override
        public ScannedDeviceInfo createFromParcel(Parcel source) {
            return new ScannedDeviceInfo(source);
        }

        @Override
        public ScannedDeviceInfo[] newArray(int size) {
            return new ScannedDeviceInfo[size];
        }
    };
}
