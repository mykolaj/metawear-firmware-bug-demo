package com.mt53bureau.fwbugdemo.model;

import com.mt53bureau.fwbugdemo.CsvFormatters;

import java.util.Calendar;
import java.util.Locale;

import static com.mt53bureau.fwbugdemo.CsvFormatters.FLOAT_FORMATTER;

public class ModuleReading {

    public final Calendar timestamp;
    public final float x;
    public final float y;
    public final float z;

    public ModuleReading(final Calendar timestamp, float x, float y, float z) {
        this.timestamp = timestamp;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static String toCsv(ModuleReading reading) {
        if (reading != null) {
            return String.format(Locale.US, "%s,%s,%s",
                    FLOAT_FORMATTER.format(reading.x),
                    FLOAT_FORMATTER.format(reading.y),
                    FLOAT_FORMATTER.format(reading.z));
        } else {
            return ",,";
        }
    }
}
