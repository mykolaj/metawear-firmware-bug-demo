package com.mt53bureau.fwbugdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.mt53bureau.fwbugdemo.enums.ConnectionStatus;

public class ItemInfoWrapper implements Parcelable {
    private final ScannedDeviceInfo mScannedDeviceInfo;
    public boolean isChecked;
    public int slotNumber;
    public ConnectionStatus connectionStatus = ConnectionStatus.UNKNOWN;
    public byte batteryCharge;
    public boolean allowCheckboxInput = true;

    public ItemInfoWrapper(final ScannedDeviceInfo deviceInfo) {
        mScannedDeviceInfo = deviceInfo;
    }

    public ScannedDeviceInfo getScannedDeviceInfo() {
        return mScannedDeviceInfo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mScannedDeviceInfo, flags);
        dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.slotNumber);
        dest.writeInt(this.connectionStatus == null ? -1 : this.connectionStatus.ordinal());
        dest.writeByte(this.batteryCharge);
    }

    protected ItemInfoWrapper(Parcel in) {
        this.mScannedDeviceInfo = in.readParcelable(ScannedDeviceInfo.class.getClassLoader());
        this.isChecked = in.readByte() != 0;
        this.slotNumber = in.readInt();
        int tmpConnectionStatus = in.readInt();
        this.connectionStatus = tmpConnectionStatus == -1 ? null : ConnectionStatus.values()[tmpConnectionStatus];
        this.batteryCharge = in.readByte();
    }

    public static final Parcelable.Creator<ItemInfoWrapper> CREATOR = new Parcelable.Creator<ItemInfoWrapper>() {
        @Override
        public ItemInfoWrapper createFromParcel(Parcel source) {
            return new ItemInfoWrapper(source);
        }

        @Override
        public ItemInfoWrapper[] newArray(int size) {
            return new ItemInfoWrapper[size];
        }
    };
}
